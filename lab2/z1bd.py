import math
import random
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from more_itertools import numeric_range
"""
d0 - średnica okręgu dla któego liczymy siłę odpychającą. Im większa tym siły odpychające przeszkody mają większy zasięg
k0 = ko - współczynnik wzmocnienia siły odpychającej. Im wyższy wym większa siła, i w efekcie jej promień
          ale ograniczony przez d0.
"""
delta = 0.05
x_size = 10
y_size = 10
kp = 0.02
d0 = dr = 3

obstaclesCoordinates = [(random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0)]
start_point=(-10, random.randint(-1000, 1000)/100.0)
finish_point=(10, random.randint(-1000, 1000)/100.0)
ko = 10


def createMatrixWithXYCoordinates():
    matrixWithXYCoordinates = list()
    row = list()
    for y in numeric_range(-10.0, 10.0, delta):
        for x in numeric_range(-10.0, 10.0, delta):
            row.append((x, y))
        matrixWithXYCoordinates.append(row)
        row = list()
    return matrixWithXYCoordinates

def SumOfForces(robotCoordinates, point, obstacles, ObstacleWeight, detectionDistanceLimit, ko):
    return SumOfRepulsiveForces(point, obstacles, ObstacleWeight, detectionDistanceLimit) \
           + AttractiveForce(point, robotCoordinates, ko)

def MatrixOfForces(matrixWithXYcoordinates, obstacles, robotCoordinates, ObstacleWeight, ko,
                            detectionDistanceLimit):
    matrixOfForces = list()
    row = list()
    for line in matrixWithXYcoordinates:
        for point in line:
            row.append(
                SumOfForces(robotCoordinates, point, obstacles, ObstacleWeight, detectionDistanceLimit, ko))
        matrixOfForces.append(row)
        row = list()
    return matrixOfForces

def DistanceBetweenPionts(firstPoint, secondPoint):
    substractedCoordinates = np.subtract(firstPoint, secondPoint)
    return math.sqrt((substractedCoordinates[0]**2) + substractedCoordinates[1]**2)

def AttractiveForce(robotCoordinates, targetCoordinates, kp):
    return kp * DistanceBetweenPionts(robotCoordinates, targetCoordinates)

def RepulsiveForce(robotCoordinates, obstacleCoordinates, ko, detectionDistanceLimit):
    distance = DistanceBetweenPionts(robotCoordinates, obstacleCoordinates)
    if distance == 0:
        return 1
    elif distance <= detectionDistanceLimit:
        return ko*(((1/distance) - 1 / detectionDistanceLimit) * (1 / (distance ** 2)))
    else:
        return 0

def SumOfRepulsiveForces(robotCoordinates, obstacles, ko, detectionDistanceLimit):
    SumOfRepulsiveForces = 0
    for obstacle in obstacles:
        SumOfRepulsiveForces += RepulsiveForce(robotCoordinates, obstacle, ko, detectionDistanceLimit)
    return SumOfRepulsiveForces

def makeVectorFromPoints(firstPoint, secondPoint):
    return np.subtract(secondPoint, firstPoint)

def VectorMagnitude(vector):
    return np.sqrt(vector.dot(vector))

def normalizeVector(vector):
    magnitude = VectorMagnitude(vector)
    return [vector[0]/magnitude, vector[1]/magnitude]

def multiplyVectorsByValue(vector, value):
    return np.multiply(vector, value)

def addVectors(firstVector, secondVector):
    return np.add(firstVector, secondVector)

def roundTo(x, base):
    return round(base*round(x/base), 2)

def makeAttractiveVector(actualPoint, finish_point, kp):
    attractiveVector = makeVectorFromPoints(actualPoint, finish_point)
    attractiveVector = normalizeVector(attractiveVector)
    attractiveForce = AttractiveForce(actualPoint, finish_point, kp)
    attractiveVector = multiplyVectorsByValue(attractiveVector, attractiveForce)
    return attractiveVector

def makeRepulsiveVectorSum(actualPoint, dr, ko):
    repulsiveVectorSum = (0, 0)
    for obstacle in obstaclesCoordinates:
        repulsiveVector = makeVectorFromPoints(actualPoint, obstacle)
        repulsiveVector = normalizeVector(repulsiveVector)
        repulsiveForce = RepulsiveForce(actualPoint, obstacle, dr, ko)
        repulsiveVector = multiplyVectorsByValue(repulsiveVector, repulsiveForce)
        repulsiveVectorSum = addVectors(repulsiveVectorSum, repulsiveVector)
    repulsiveVectorSum = multiplyVectorsByValue(repulsiveVectorSum, -1.0)
    return repulsiveVectorSum


def makeDirectionVector(attractiveVector, repulsiveVectorSum):
    directionVector = addVectors(attractiveVector, repulsiveVectorSum)
    directionVector = normalizeVector(directionVector)
    return directionVector


def roundVectorToCoordinates(directionVector, delta):
    dx = roundTo(directionVector[0] * delta, delta)
    dy = roundTo(directionVector[1] * delta, delta)
    return dx, dy


def moveToNextCoordinates(actualPoint, dx, dy):
    x = actualPoint[0] + dx
    y = actualPoint[1] + dy
    if x > 10:
        x = 10
    if x < -10:
        x = -10
    if y > 10:
        y = 10
    if y < -10:
        y = -10
    return x, y

def makePath(start_point, finish_point, kp, ko, dr, delta):
    path = list()
    actualPoint = start_point
    for i in range(2000):
        if actualPoint == finish_point:
            break

        attractiveVector = makeAttractiveVector(actualPoint, finish_point, kp)
        repulsiveVectorSum = makeRepulsiveVectorSum(actualPoint, dr, ko)
        directionVector = makeDirectionVector(attractiveVector, repulsiveVectorSum)

        dx, dy = roundVectorToCoordinates(directionVector, delta)

        actualPoint = moveToNextCoordinates(actualPoint, dx, dy)
        path.append(actualPoint)
    return path




coordinatesMatrix = createMatrixWithXYCoordinates()
Z = MatrixOfForces(coordinatesMatrix, obstaclesCoordinates, finish_point, ko, kp, d0)

path = makePath(start_point, finish_point, kp, ko, dr, delta)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn, origin='lower',
           extent=[-x_size, x_size, -y_size, y_size], vmax=1)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obstaclesCoordinates:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for actualPoint in path:
    plt.plot(actualPoint[0], actualPoint[1], "or", color='yellow', markersize=1)

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()

