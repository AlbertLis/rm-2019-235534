import math
import random
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from more_itertools import numeric_range
"""
d0 - średnica okręgu dla któego liczymy siłę odpychającą. Im większa tym siły odpychające przeszkody mają większy zasięg
k0 = ko - współczynnik wzmocnienia siły odpychającej. Im wyższy wym większa siła, i w efekcie jej promień
          ale ograniczony przez d0.
"""
delta = 0.05
x_size = 10
y_size = 10
kp = 0.001
d0 = 1

obstaclesCoordinates = [(random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0),
                        (random.randint(-1000, 1000)/100.0, random.randint(-1000, 1000)/100.0)]
start_point=(-10, random.randint(-1000, 1000)/100.0)
finish_point=(10, random.randint(-1000, 1000)/100.0)
ko = 10


def createMatrixWithXYCoordinates():
    matrixWithXYCoordinates = list()
    row = list()
    for y in numeric_range(-10.0, 10.0, delta):
        for x in numeric_range(-10.0, 10.0, delta):
            row.append((x, y))
        matrixWithXYCoordinates.append(row)
        row = list()
    return matrixWithXYCoordinates

def calculateSumOfForces(robotCoordinates, point, obstacles, ObstacleWeight, detectionDistanceLimit, ko):
    return calculateSumOfRepulsiveForces(point, obstacles, ObstacleWeight, detectionDistanceLimit) \
           + calculateAttractiveForce(point, robotCoordinates, ko)

def calculateMatrixOfForces(matrixWithXYcoordinates, obstacles, robotCoordinates, ObstacleWeight, ko,
                            detectionDistanceLimit):
    matrixOfForces = list()
    row = list()
    for line in matrixWithXYcoordinates:
        for point in line:
            row.append(
                calculateSumOfForces(robotCoordinates, point, obstacles, ObstacleWeight, detectionDistanceLimit, ko))
        matrixOfForces.append(row)
        row = list()
    return matrixOfForces

def calculateDistanceBetweenPionts(firstPoint, secondPoint):
    substractedCoordinates = np.subtract(firstPoint, secondPoint)
    return math.sqrt((substractedCoordinates[0]**2) + substractedCoordinates[1]**2)

def calculateAttractiveForce(robotCoordinates, targetCoordinates, kp):
    return kp * calculateDistanceBetweenPionts(robotCoordinates, targetCoordinates)

def calculateRepulsiveForce(robotCoordinates, obstacleCoordinates, ko, detectionDistanceLimit):
    distance = calculateDistanceBetweenPionts(robotCoordinates, obstacleCoordinates)
    if distance == 0:
        return 1
    elif distance <= detectionDistanceLimit:
        return ko*(((1/distance) - 1 / detectionDistanceLimit) * (1 / (distance ** 2)))
    else:
        return 0

def calculateSumOfRepulsiveForces(robotCoordinates, obstacles, ko, detectionDistanceLimit):
    SumOfRepulsiveForces = 0
    for obstacle in obstacles:
        SumOfRepulsiveForces += calculateRepulsiveForce(robotCoordinates, obstacle, ko, detectionDistanceLimit)
    return SumOfRepulsiveForces

coordinatesMatrix = createMatrixWithXYCoordinates()
Z = calculateMatrixOfForces(coordinatesMatrix, obstaclesCoordinates, finish_point, ko, kp, d0)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn, origin='lower',
           extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obstaclesCoordinates:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()

